<?php
/**
 * Plugin Name: Customize Switch Control
 * Plugin URI:  https://gitlab.com/S8_kuuak/supt-customize-switch-control
 * Description: Custom switch control for the WordPress Customizer.
 * Author:      Felipe (@kuuak)
 * Author URI:  https://profiles.wordpress.org/kuuak/
 * Version:     1.0.0
 * License:     GNU General Public License v3 or later
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

add_action('init', 'supt_register_customize_switch_control' );

function supt_register_customize_switch_control() {

	if ( !class_exists('WP_Customize_Control') ) return;
	class SUPT_Customize_Switch_Control extends WP_Customize_Control {
		public $type = 'switch';

		public function enqueue() {
			wp_enqueue_style( 'supt_customize_switch_control', plugins_url('supt-switch-control.css', __FILE__)	 );
		}

		public function render_content() {
			printf(
				'<div class="supt-customize-switch-control">
					<input class="supt-customize-switch-control__chk" type="checkbox" id="%1$s" value="%2$s" %3$s>
					<label for="%1$s" class="supt-customize-switch-control__label">
						<span>%4$s</span>
					</label>
					<p class="supt-customize-switch-control__desc">%5$s</p>
				</div>',
				esc_attr( $this->id ),
				esc_attr( $this->value() ),
				$this->get_link() .' '. checked( $this->value(), true, false ),
				esc_html( $this->label ),
				wp_kses_post( $this->description )
			);
		}
	}

}

